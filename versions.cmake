# This maintains the links for all sources used by this superbuild.
# Simply update this file to change the revision.
# One can use different revision on different platforms.
# e.g.
# if (UNIX)
#   ..
# else (APPLE)
#   ..
# endif()

add_revision(zlib
  URL "http://www.paraview.org/files/dependencies/zlib-1.2.7.tar.gz"
  URL_MD5 60df6a37c56e7c1366cca812414f7b85)
# NOTE: if updating zlib version, fix patch in zlib.cmake


add_revision(png
  URL "http://paraview.org/files/dependencies/libpng-1.4.8.tar.gz"
  URL_MD5 49c6e05be5fa88ed815945d7ca7d4aa9)

add_revision(freetype
  URL "http://paraview.org/files/dependencies/freetype-2.4.8.tar.gz"
  URL_MD5 "5d82aaa9a4abc0ebbd592783208d9c76")

add_revision(szip
  URL "http://paraview.org/files/dependencies/szip-2.1.tar.gz"
  URL_MD5 902f831bcefb69c6b635374424acbead)

add_revision(hdf5
  URL "http://www.paraview.org/files/dependencies/hdf5-1.8.13.tar.gz"
  URL_MD5 c03426e9e77d7766944654280b467289)

add_revision(silo
  URL "http://paraview.org/files/dependencies/silo-4.9.1-bsd.tar.gz"
  URL_MD5 465d2a0a8958b088cde83fb2a5a7eeef)

add_revision(cgns
  URL "http://www.paraview.org/files/dependencies/cgnslib_3.1.3-4.tar.gz"
  URL_MD5 442bba32b576f3429cbd086af43fd4ae)

add_revision(ffmpeg
  URL "http://paraview.org/files/dependencies/ffmpeg-2.3.3.tar.bz2"
  URL_MD5 72361d3b8717b6db3ad2b9da8df7af5e)

add_revision(libxml2
  URL "http://paraview.org/files/dependencies/libxml2-2.7.8.tar.gz"
  URL_MD5 8127a65e8c3b08856093099b52599c86)

add_revision(fontconfig
  URL "http://paraview.org/files/dependencies/fontconfig-2.8.0.tar.gz"
  URL_MD5 77e15a92006ddc2adbb06f840d591c0e)

add_revision(qt
  URL "http://paraview.org/files/dependencies/qt-everywhere-opensource-src-4.8.6.tar.gz"
  URL_MD5 2edbe4d6c2eff33ef91732602f3518eb)

add_revision(boost
  URL "http://sourceforge.net/projects/boost/files/boost/1.56.0/boost_1_56_0.tar.gz/download"
  URL_MD5 8c54705c424513fa2be0042696a3a162)

add_revision(manta
  URL "http://paraview.org/files/dependencies/manta-r2439.tar.gz"
  URL_MD5 fbf4107fe2f6d7e8a5ae3dda71805bdc)

add_revision(molequeue
  GIT_REPOSITORY "git://source.openchemistry.org/molequeue.git"
  GIT_TAG e545ee25b4b79d5bae9e1)

if (UNIX)
  add_revision(mpi
    URL "http://paraview.org/files/dependencies/mpich2-1.4.1p1.tar.gz"
    URL_MD5 b470666749bcb4a0449a072a18e2c204)
elseif (WIN32)
  add_revision(mpi
    URL "http://www.paraview.org/files/dependencies/openmpi-1.4.4.tar.gz"
    URL_MD5 7253c2a43445fbce2bf4f1dfbac113ad)
endif()

if (CROSS_BUILD_STAGE STREQUAL "CROSS")
  add_revision(mesa
    URL "http://www.paraview.org/files/dependencies/MesaLib-7.6.1.tar.gz"
    URL_MD5 e80fabad2e3eb7990adae773d6aeacba)
else()
  add_revision(mesa
    URL "http://paraview.org/files/dependencies/MesaLib-7.11.2.tar.gz"
    URL_MD5 b9e84efee3931c0acbccd1bb5a860554)
endif()

# We stick with 7.11.2 for Mesa version for now. Newer mesa doesn't seem to
# build correctly with certain older compilers (e.g. on neser).
add_revision(osmesa
    URL "http://paraview.org/files/dependencies/MesaLib-7.11.2.tar.gz"
    URL_MD5 b9e84efee3931c0acbccd1bb5a860554)

if(USE_PARAVIEW_master)
  add_revision(paraview
    GIT_REPOSITORY "https://gitlab.kitware.com/paraview/paraview.git"
    GIT_TAG master)
else()
  add_revision(paraview
    # updating to ParaView master of May 11, 2015
    GIT_REPOSITORY "https://gitlab.kitware.com/paraview/paraview.git"
    GIT_TAG e35c150633c629837482843ad1ed03978cda267a)
endif()

#------------------------------------------------------------------------------
# CMB versions
#------------------------------------------------------------------------------
#use the new cmb_v4 repo for cmb
option(CMB_FROM_GIT "Build CMB from git" ON)
if (CMB_FROM_GIT)
  # Download CMB from GIT
  add_customizable_revision(cmb
    GIT_REPOSITORY https://gitlab.kitware.com/cmb/cmb.git
    GIT_TAG "master")
else()
  if (CMB_FROM_SOURCE_DIR)
    add_customizable_revision(cmb
      SOURCE_DIR "CMBSource")
  else()
    message(FATAL_ERROR "No tarballs released yet")
    # Variables to hold the URL and MD5 (optional)
    #add_customizable_revision(cmb
    #  URL "http://www.paraview.org/files/v4.3/ParaView-v4.3.1-source.tar.gz"
    #  URL_MD5 "d03d3ab504037edd21306413dff64293")
  endif()
endif()

add_revision(vxl
  GIT_REPOSITORY "https://github.com/vxl/vxl"
  GIT_TAG 62f6b765b8d902b24177b0c6f60c799c28ac7891)

if (WIN32)
  #windows uses the custom cmake build of zeroMQ
  add_revision(zeroMQ
    GIT_REPOSITORY "https://github.com/robertmaynard/zeromq2-x"
    GIT_TAG master)
else()
  #unix and mac uses the stable release
  add_revision(zeroMQ
    URL "http://download.zeromq.org/zeromq-2.2.0.tar.gz"
    URL_MD5 1b11aae09b19d18276d0717b2ea288f6)

endif()

#Use master of remus to get fixes for cmb v4
add_revision(remus
  GIT_REPOSITORY "https://github.com/Kitware/Remus.git"
  GIT_TAG master)

add_revision(kml
  URL "http://vtk.org/files/support/libkml_fa6c7d8.tar.gz"
  URL_MD5 261b39166b18c2691212ce3495be4e9c
  )

add_revision(gdal
  GIT_REPOSITORY "https://github.com/mathstuf/gdal-svn"
  GIT_TAG gdal-1.11-cmb
  )

#------------------------------------------------------------------------------
# SMTK versions
#------------------------------------------------------------------------------
#use smtk master so that we get v4 updates
option(SMTK_FROM_GIT "Build SMTK from git" ON)
if (SMTK_FROM_GIT)
  set(smtk_tag "release-v1")
  if (USE_PARAVIEW_master)
    set(smtk_tag "master")
  endif ()

  # Download SMTK from GIT
  add_customizable_revision(smtk
    GIT_REPOSITORY "https://github.com/Kitware/SMTK.git"
    GIT_TAG "${smtk_tag}")
else()
  if (SMTK_FROM_SOURCE_DIR)
    add_customizable_revision(smtk
      SOURCE_DIR "SMTKSource")
  else()
    message(FATAL_ERROR "No tarballs released yet")
    # Variables to hold the URL and MD5 (optional)
    #add_customizable_revision(smtk
    #  URL "http://www.paraview.org/files/v4.3/ParaView-v4.3.1-source.tar.gz"
    #  URL_MD5 "d03d3ab504037edd21306413dff64293")
  endif()
endif()

add_revision(shiboken
  GIT_REPOSITORY "https://github.com/OpenGeoscience/shiboken.git"
  GIT_TAG smtk-head)

#------------------------------------------------------------------------------
# moab versions
#------------------------------------------------------------------------------
add_revision(ftgl
  GIT_REPOSITORY "https://github.com/ulrichard/ftgl.git"
  GIT_TAG cf4d9957930e41c3b82a57b20207242c7ef69f18
  )

add_revision(oce
  GIT_REPOSITORY "https://github.com/robertmaynard/oce.git"
  GIT_TAG "cgm_support"
  )

add_revision(netcdf
  URL "ftp://ftp.unidata.ucar.edu/pub/netcdf/old/netcdf-4.3.0.tar.gz"
  URL_MD5 40c0e53433fc5dc59296ee257ff4a813
  )

add_revision(netcdfcpp
  URL "ftp://ftp.unidata.ucar.edu/pub/netcdf/netcdf-cxx-4.2.tar.gz"
  URL_MD5 d32b20c00f144ae6565d9e98d9f6204c
)

add_revision(cgm
  GIT_REPOSITORY "https://bitbucket.org/fathomteam/cgm.git"
  GIT_TAG 13.1.1
  )

add_revision(moab
  GIT_REPOSITORY "https://github.com/judajake/moab.git"
  GIT_TAG "624c4e95ad1e935a7937a1b10bf252632d4496d1"
  )

#------------------------------------------------------------------------------
# Optional Plugins. Doesn't affect ParaView binaries at all even if missing
# or disabled.
#------------------------------------------------------------------------------

if (USE_NONFREE_COMPONENTS)
  add_revision(qhull
    GIT_REPOSITORY git://github.com/gzagaris/gxzagas-qhull.git
    GIT_TAG master)

  add_revision(genericio
    GIT_REPOSITORY git://kwsource.kitwarein.com/genericio/genericio.git
    GIT_TAG master)

  # Add an option to not use diy from SVN. On Debian-Etch the SVN is too old
  # to work with invalid SVN certificates.
  option(DIY_SKIP_SVN "If enabled, we simply download diy from a source tar" OFF)
  if(DIY_SKIP_SVN)
    add_revision(diy
      URL "http://paraview.org/files/dependencies/diy-src.r178.tar.gz"
      URL_MD5 4fba13aae93927d0f32dd6db0599ffcd)
  else()
    if (TRUST_SVN_CERTIFICATES_AUTOMATICALLY)
      add_revision(diy
         SVN_REPOSITORY https://svn.mcs.anl.gov/repos/diy/trunk
         SVN_REVISION -r178
         SVN_TRUST_CERT 1)
    else()
      add_revision(diy
         SVN_REPOSITORY https://svn.mcs.anl.gov/repos/diy/trunk
         SVN_REVISION -r178)
    endif()
  endif()

  add_revision(cosmotools
    GIT_REPOSITORY git://public.kitware.com/cosmotools.git
    GIT_TAG v0.13)

  add_revision(acusolve
    GIT_REPOSITORY git://kwsource.kitwarein.com/paraview/acusolvereaderplugin.git
    GIT_TAG master)

  add_revision(vistrails
    GIT_REPOSITORY git://kwsource.kitwarein.com/paraview/vistrails.git
    GIT_TAG master)

  add_revision(portfwd
    URL "http://www.paraview.org/files/dependencies/portfwd-0.29.tar.gz"
    URL_MD5 93161c91e12b0d67ca52dc13708a2f2f)

  add_revision(triangle
    GIT_REPOSITORY "https://github.com/robertmaynard/triangle.git"
    GIT_TAG master)
endif ()
