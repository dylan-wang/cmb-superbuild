
#reset back the c/cppflag to the pre netcdf values.
#this works since the only project that depends on netcdf is moab
if (build-projects)
  set (cppflags "${pre_netcdf_cpp_flags}")
  set (cflags "${pre_netcdf_c_flags}")
endif()

#set(cgm_find_flags )
#if(cgm_ENABLED)
#  set(cgm_find_flags "-DCGM_CFG:PATH=${SuperBuild_BINARY_DIR}/cgm/src/cgm/cgm.make")
#endif()
add_external_project(moab
  DEPENDS hdf5 netcdfcpp
#  DEPENDS_OPTIONAL cgm
  CMAKE_ARGS
    -DBUILD_SHARED_LIBS:BOOL=OFF
    -DMOAB_USE_SZIP:BOOL=ON
    -DMOAB_USE_CGM:BOOL=OFF
    -DMOAB_USE_CGNS:BOOL=OFF
    -DMOAB_USE_MPI:BOOL=OFF
    -DMOAB_USE_HDF:BOOL=ON
    -DMOAB_USE_NETCDF:BOOL=ON
    -DMOAB_ENABLE_TESTING:BOOL=ON #build can't handle this being disabled
    ${cgm_find_flags}
)
