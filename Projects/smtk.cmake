set(paraview_dir ${SuperBuild_BINARY_DIR}/paraview/src/paraview-build)

if (__BUILDBOT_INSTALL_LOCATION)
  set(paraview_dir <INSTALL_DIR>/lib/cmake/paraview-4.3)
endif ()

if (WIN32)
  # On Windows we expect the Python source for module to be
  # in a different place than Unix builds and in a different
  # place than SMTK would put it by default. Tell SMTK where
  # to install Python source for the smtk module:
  list(APPEND extra_cmake_args "-DSMTK_PYTHON_MODULEDIR:PATH=${SuperBuild_BINARY_DIR}/install/python")
endif()

set(smtk_libdir lib)
if (WIN32)
  set(smtk_libdir bin)
endif ()

add_external_project_or_just_build_dependencies(smtk
  DEPENDS boost qt shiboken paraview remus
  CMAKE_ARGS
    ${extra_cmake_args}
    "-C${CMAKE_BINARY_DIR}/env.cmake"
    -DBUILD_SHARED_LIBS:BOOL=ON

    -DSMTK_ENABLE_QT_SUPPORT:BOOL=ON
    -DSMTK_ENABLE_VTK_SUPPORT:BOOL=ON
    -DSMTK_ENABLE_PARAVIEW_SUPPORT:BOOL=ON
    -DSMTK_ENABLE_DISCRETE_SESSION:BOOL=ON
    -DSMTK_ENABLE_EXODUS_SESSION:BOOL=ON
    -DSMTK_ENABLE_REMOTE_SESSION:BOOL=ON
    -DSMTK_ENABLE_REMUS_SUPPORT:BOOL=ON
    -DSMTK_ENABLE_PYTHON_WRAPPING:BOOL=${shiboken_ENABLED}
    -DCMAKE_INSTALL_LIBDIR:STRING=${smtk_libdir}

    -DParaView_DIR:PATH=${paraview_dir}
    -DBOOST_INCLUDEDIR:PATH=<INSTALL_DIR>/include/boost
    -DBOOST_LIBRARYDIR:PATH=<INSTALL_DIR>/lib
)

if(shiboken_ENABLED)
  add_external_project_step(install_shiboken_python_plugin
    COMMENT "Fixing missing include files."
    COMMAND  ${CMAKE_COMMAND}
      -DCMAKE_BUILD_TYPE:STRING=${CMAKE_BUILD_TYPE}
      -Dpv_version:STRING=${pv_version}
      -Dpv_python_executable:PATH=${pv_python_executable}
      -DBUILD_SHARED_LIBS:BOOL=ON
      -DSMTK_BIN_DIR:PATH=${install_location}
      -DTMP_DIR:PATH=<TMP_DIR>
      -P ${CMAKE_CURRENT_LIST_DIR}/install_smtk_python_plugin.cmake
    DEPENDEES install)
endif()

add_extra_cmake_args(
  -DSMTK_DIR:PATH=<INSTALL_DIR>/lib/cmake/SMTK)
