add_external_project(vxl
  DEPENDS png
  PATCH_COMMAND ${CMAKE_COMMAND} -E copy_if_different
                ${SuperBuild_PROJECTS_DIR}/patches/vxl.core.vul.vul_psfile.h
                <SOURCE_DIR>/core/vul/vul_psfile.h
  CMAKE_ARGS
    #Needed to make sure we find the correct png lib/includes
    -DCMAKE_FIND_FRAMEWORK=LAST

    # Options turned ON
    -DBUILD_CONTRIB=ON
    -DBUILD_CORE_GEOMETRY=ON
    -DBUILD_CORE_IMAGING=ON
    -DBUILD_CORE_NUMERICS=ON
    -DBUILD_CORE_SERIALISATION=ON
    -DBUILD_CORE_UTILITIES=ON
    -DBUILD_MUL=ON
    -DBUILD_OXL=ON
    -DBUILD_RPL=ON
    -DBUILD_RPL_RGTL=ON

    # Options turned OFF
    -DBUILD_BRL=OFF
    -DBUILD_CONVERSIONS=OFF
    -DBUILD_CORE_VIDEO=OFF
    -DBUILD_EXAMPLES=OFF
    -DBUILD_FOR_VXL_DASHBOARD=OFF
    -DBUILD_GEL=OFF
    -DBUILD_MUL_TOOLS=OFF
    -DBUILD_OUL=OFF
    -DBUILD_OXL=OFF
    -DBUILD_PRIP=OFF
    -DBUILD_SHARED_LIBS=OFF
    -DBUILD_TBL=OFF
    -DBUILD_TESTING=OFF
    -DBUILD_UNMAINTAINED_LIBRARIES=OFF
    -DBUILD_VGUI=OFF
    -DCMAKE_INSTALL_PREFIX=<INSTALL_DIR>
)

add_extra_cmake_args(
  -DVXL_DIR:PATH=<INSTALL_DIR>/share/vxl/cmake
)
