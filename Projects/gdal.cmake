add_external_project(gdal
  DEPENDS zlib
  CMAKE_ARGS
    -DGDAL_USE_CURL:BOOL=OFF
    -DGDAL_USE_LIBJPEG_INTERNAL:BOOL=ON
    -DGDAL_USE_LIBTIFF_INTERNAL:BOOL=ON
    -DGDAL_ENABLE_FRMT_PDF:BOOL=OFF
    -DGDAL_ENABLE_FRMT_VRT:BOOL=ON
    -DOGR_ENABLE_SHP:BOOL=ON)

if (APPLE)
  set(gdal_lib <INSTALL_DIR>/lib/libgdal111.dylib)
elseif (WIN32)
  set(gdal_lib <INSTALL_DIR>/lib/gdal111.lib)
else ()
  set(gdal_lib <INSTALL_DIR>/lib/libgdal111.so)
endif ()

add_extra_cmake_args(
  -DGDAL_INCLUDE_DIR:FILEPATH=<INSTALL_DIR>/include/gdal
  -DGDAL_LIBRARY:FILEPATH=${gdal_lib})
