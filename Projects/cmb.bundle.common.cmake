# Consolidates platform independent stub for cmb.bundle.cmake files.

include (cmb_version)
include (paraview_version)

# Enable CPack packaging.
set(CPACK_PACKAGE_DESCRIPTION_SUMMARY
  "CMB Suite of Tools.")
set(CPACK_PACKAGE_NAME "CMB")
set(CPACK_PACKAGE_VENDOR "Kitware, Inc.")
set(CPACK_RESOURCE_FILE_LICENSE
    "${CMBSuperBuild_SOURCE_DIR}/License.txt")
set(CPACK_PACKAGE_VERSION_MAJOR ${cmb_version_major})
set(CPACK_PACKAGE_VERSION_MINOR ${cmb_version_minor})
if (cmb_version_suffix)
  set(CPACK_PACKAGE_VERSION_PATCH ${cmb_version_patch}-${cmb_version_suffix})
else()
  set(CPACK_PACKAGE_VERSION_PATCH ${cmb_version_patch})
endif()

SET(CPACK_PACKAGE_INSTALL_DIRECTORY
  "CMBSuite ${CPACK_PACKAGE_VERSION_MAJOR}.${CPACK_PACKAGE_VERSION_MINOR}.${CPACK_PACKAGE_VERSION_PATCH}")
set(CPACK_SOURCE_PACKAGE_FILE_NAME
  "CMBSuite-${CPACK_PACKAGE_VERSION_MAJOR}.${CPACK_PACKAGE_VERSION_MINOR}.${CPACK_PACKAGE_VERSION_PATCH}")

if(NOT DEFINED CPACK_SYSTEM_NAME)
  set(CPACK_SYSTEM_NAME ${CMAKE_SYSTEM_NAME}-${CMAKE_SYSTEM_PROCESSOR})
endif()
if(${CPACK_SYSTEM_NAME} MATCHES Windows)
  if(CMAKE_CL_64)
    set(CPACK_SYSTEM_NAME win64)
  else(CMAKE_CL_64)
    set(CPACK_SYSTEM_NAME win32)
  endif()
endif()
if(NOT DEFINED CPACK_PACKAGE_FILE_NAME)
  set(CPACK_PACKAGE_FILE_NAME
    "${CPACK_SOURCE_PACKAGE_FILE_NAME}-${CPACK_SYSTEM_NAME}")
endif()

#set the programs we want to install
set(cmb_programs_to_install
    ModelBuilder
    paraview
    )


#sort the list so the package order is reasonable and deducible to the user
list(SORT cmb_programs_to_install)

# Don't import CPack yet, let the platform specific code get another chance at
# changing the variables.


# PARAVIEW_INSTALL_MANUAL_PDF is set before importing this file.
# This allows us to override the pdf downloading code for apple.
if (PARAVIEW_INSTALL_MANUAL_PDF)
  set (pdf_pv_version "4.0")
  # download an install manual pdf.
  install(CODE "
    # create the doc directory.
    file(MAKE_DIRECTORY \"\${CMAKE_INSTALL_PREFIX}/doc\")

    # download the manual pdf.
    file(DOWNLOAD \"http://www.paraview.org/files/v${pdf_pv_version}/ParaViewManual.v${pdf_pv_version}.pdf\"
        \"\${CMAKE_INSTALL_PREFIX}/doc/ParaViewManual.v${pv_version}.pdf\"
        SHOW_PROGRESS)
  ")
endif()
