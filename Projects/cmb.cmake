set(paraview_dir ${SuperBuild_BINARY_DIR}/paraview/src/paraview-build)
set(vtk_dir ${SuperBuild_BINARY_DIR}/paraview/src/paraview-build/VTK)
set(smtk_dir ${SuperBuild_BINARY_DIR}/install/lib/cmake/SMTK)

if (__BUILDBOT_INSTALL_LOCATION)
  set(paraview_dir <INSTALL_DIR>/lib/cmake/paraview-4.3)
  set(smtk_dir <INSTALL_DIR>/lib/cmake/SMTK)
  set(vtk_dir <INSTALL_DIR>/lib/cmake/paraview-4.3)
endif ()

add_external_project_or_just_build_dependencies(cmb
  DEPENDS boost remus vxl kml gdal qt python paraview molequeue smtk
  DEPENDS_OPTIONAL moab triangle
  CMAKE_ARGS
    ${extra_cmake_args}
    -DKML_DIR:PATH=<INSTALL_DIR>
    -DGDAL_DIR:PATH=<INSTALL_DIR>
    -DParaView_DIR:PATH=${paraview_dir}
    -DSMTK_DIR:PATH=${smtk_dir}
    -DVTK_DIR:PATH=${vtk_dir}
    -DMoleQueue_DIR:PATH=<INSTALL_DIR>
    -DCMB_TEST_PLUGIN_PATHS:STRING=<INSTALL_DIR>/${smtk_libdir}

    #specify semi-colon separated paths for session plugins
    -DCMB_TEST_PLUGIN_PATHS:STRING=<INSTALL_DIR>/lib
    #specify what mesh workers we should build
    -DBUILD_TRIANGLE_MESH_WORKER:BOOL=${triangle_ENABLED}

    # specify the apple app install prefix. No harm in specifying it for all
    # platforms.
    -DMACOSX_APP_INSTALL_PREFIX:PATH=<INSTALL_DIR>/Applications
)

#special mac only script to install plugin for paraview
if(APPLE)
  include(cmb_version)
  add_external_project_step(install_cmb_paraview_plugin
    COMMENT "Fixing missing include files."
    COMMAND  ${CMAKE_COMMAND}
      -DBUILD_SHARED_LIBS:BOOL=ON
      -DINSTALL_DIR:PATH=<INSTALL_DIR>
      -DCMB_BINARY_DIR:PATH=${SuperBuild_BINARY_DIR}/cmb/src/cmb-build
      -DTMP_DIR:PATH=<TMP_DIR>
      -DCMB_VERSION:STRING=${cmb_version}
      -P ${CMAKE_CURRENT_LIST_DIR}/apple/install_cmb_paraview_plugin.cmake
    DEPENDEES install)

  #special mac only script to install smtk plugins for cmb
  add_external_project_step(install_smtk_cmb_plugins
    COMMENT "installing smtk plugins for cmb."
    COMMAND  ${CMAKE_COMMAND}
      -DCMAKE_BUILD_TYPE:STRING=${CMAKE_BUILD_TYPE}
      -DBUILD_SHARED_LIBS:BOOL=ON
      -DINSTALL_DIR:PATH=<INSTALL_DIR>
      -DSMTK_BIN_DIR:PATH=${install_location}
      -DTMP_DIR:PATH=<TMP_DIR>
      -P ${CMAKE_CURRENT_LIST_DIR}/apple/install_smtk_cmb_plugin.cmake
    DEPENDEES install)

endif()
