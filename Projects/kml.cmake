add_external_project(kml
  # kml has problems with newer gcc compilers with not including unistd.h
  PATCH_COMMAND ${CMAKE_COMMAND} -E copy_if_different
                ${SuperBuild_PROJECTS_DIR}/patches/kml.src.kml.base.file_posix.cc
                <SOURCE_DIR>/src/kml/base/file_posix.cc
  CMAKE_ARGS
    -DBUILD_SHARED_LIBS=OFF
  )

if (WIN32)
  # Patch for MSVC2010+
  add_external_project_step(kml-patch-util-header
    COMMAND ${CMAKE_COMMAND} -E copy_if_different
                              ${SuperBuild_PROJECTS_DIR}/patches/kml.src.kml.base.util.h
                              <SOURCE_DIR>/src/kml/base/util.h
    DEPENDEES configure
    DEPENDERS build)
endif ()
