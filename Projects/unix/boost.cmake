
if(64bit_build)
  set(am 64)
else()
  set(am 32)
endif()

set(boost_with_args --with-date_time --with-filesystem --with-system --with-thread)

set(boost_toolset)
if(CMAKE_CXX_COMPILER_ID STREQUAL "Clang")
  set(boost_toolset "toolset=clang")
endif()

#since we don't specify a prefix for the superbuild,
#we can determine where the buld directory will be. This
#is needed as we need to wrap the build directory in quotes to
#and escape spaces in the path for boost to properly build.
string(REPLACE " " "\\ " boost_build_dir ${SuperBuild_BINARY_DIR}/boost/src/boost)
string(REPLACE " " "\\ " boost_install_dir ${install_location})

add_external_project(boost
  DEPENDS zlib
  CONFIGURE_COMMAND ./bootstrap.sh ${boost_toolset} --prefix=${boost_install_dir} --libdir=${install_location}/lib
  BUILD_COMMAND ./b2 ${boost_toolset} --build-dir=${boost_build_dir} address-model=${am} dll-path=${install_location}/lib ${boost_with_args}
  INSTALL_COMMAND ./b2 ${boost_toolset} address-model=${am} ${boost_with_args} dll-path=${install_location}/lib install
  BUILD_IN_SOURCE 1
  )

add_extra_cmake_args(
  -DBOOST_ROOT:PATH=${install_location}
  -DBOOST_INCLUDEDIR:PATH=<INSTALL_DIR>/include/boost
  -DBOOST_LIBRARYDIR:PATH=${install_location}/lib
  -DBoost_NO_SYSTEM_PATHS:BOOL=True
  -DBoost_NO_BOOST_CMAKE:BOOL=True
  -DBoost_USE_STATIC_LIBS:BOOL=False
  -DBoost_USE_STATIC_RUNTIME:BOOL=False
)
