# script to "bundle" cmb for unix, unlike paraview we are going to dump everything
# into bin instead of having a clear separation of bin and lib.  We have too many
# libraries and executables that have this hardcoded concept that the libraries are
# directly beside the executable

include(cmb.bundle.common)
include(CPack)

# install all ParaView's shared libraries.
install(DIRECTORY "${install_location}/lib/paraview-${pv_version}"
  DESTINATION "lib"
  USE_SOURCE_PERMISSIONS
  COMPONENT superbuild)

# install all cmb's shared libraries.
install(DIRECTORY "${install_location}/lib/cmb-${cmb_version}"
  DESTINATION "lib"
  USE_SOURCE_PERMISSIONS
  COMPONENT superbuild)

#get the right version of paraview projects
foreach(prog ${cmb_programs_to_install})
  if(prog STREQUAL "paraview")
    set(programs_path ${install_location}/lib/paraview-${pv_version}/${prog})
  else()
    set(programs_path ${install_location}/lib/cmb-${cmb_version}/${prog})
  endif()

  install(CODE
      "execute_process(COMMAND
      ${CMAKE_COMMAND}
        -Dexecutable:PATH=${programs_path}
        -Ddependencies_root:PATH=${install_location}
        -Dpv_libraries_root:PATH=${install_location}/lib/paraview-${pv_version}
        -Dcmb_libraries_root:PATH=${install_location}/lib/cmb-${cmb_version}
        -Dtarget_root:PATH=\${CMAKE_INSTALL_PREFIX}/lib/paraview-${pv_version}
        -P ${CMAKE_CURRENT_LIST_DIR}/install_dependencies.cmake)"
    COMPONENT superbuild)
endforeach()

#install qt
if (qt_ENABLED AND NOT USE_SYSTEM_qt)
    install(DIRECTORY
      # install all qt plugins (including sqllite).
      # FIXME: we can reconfigure Qt to be built with inbuilt sqllite support to
      # avoid the need for plugins.
      "${install_location}/plugins/"
      DESTINATION "bin"
      COMPONENT superbuild
      PATTERN "*.a" EXCLUDE
      PATTERN "paraview-${pv_version}" EXCLUDE
      PATTERN "${program}-${cmb_version}" EXCLUDE
      PATTERN "fontconfig" EXCLUDE
      PATTERN "*.jar" EXCLUDE
      PATTERN "*.debug.*" EXCLUDE
      PATTERN "libboost*" EXCLUDE)
  endif()

# install executables
set (executables pvserver pvdataserver pvrenderserver)
if (python_ENABLED)
  set (executables ${executables} pvbatch pvpython)
  # we are not building pvblot for now. Disable it.
  # set (executables ${executables} pvblot)
endif()
if (qt_ENABLED)
  set (executables ${executables} paraview)
endif()

foreach(executable ${executables})
  install(PROGRAMS "${install_location}/bin/${executable}"
    DESTINATION "bin"
    COMPONENT superbuild)
endforeach()

#we have to install everything in bin that is an executable that we care about
#we use
install(DIRECTORY
    "${install_location}/bin/"
    DESTINATION "bin"
    USE_SOURCE_PERMISSIONS
    COMPONENT superbuild
    FILES_MATCHING
    REGEX "CMB.*"
    PATTERN "ModelBuilder"
    PATTERN "mesh"
    PATTERN "model"
    PATTERN "molequeue"
    PATTERN "shiboken"
    )

#add the installer as a test
add_test(NAME GenerateCMBPackage
         COMMAND ${CMAKE_CPACK_COMMAND} -G TGZ ${test_build_verbose}
         WORKING_DIRECTORY ${SuperBuild_BINARY_DIR})
set_tests_properties(GenerateCMBPackage PROPERTIES
                     LABELS "CMB"
                     TIMEOUT 1200)
