set (extra_cmake_args)
if (manta_ENABLED)
  list (APPEND extra_cmake_args
    -DMANTA_BUILD:PATH=${SuperBuild_BINARY_DIR}/manta/src/manta-build)
endif()
if(PV_NIGHTLY_SUFFIX)
  list (APPEND extra_cmake_args
    -DPV_NIGHTLY_SUFFIX:STRING=${PV_NIGHTLY_SUFFIX})
endif()

if (USE_OPENGL2_RENDERING)
  list(APPEND extra_cmake_args -DVTK_RENDERING_BACKEND:STRING=OpenGL2)
    set(_disable_plugins
      SciberQuestToolKit
      PointSprite
      NonOrthogonalSource
      PacMan
      StreamingParticles
      SierraPlotTools
      SLACTools
      UncertaintyRendering
      SurfaceLIC
      EyeDomeLighting
      RGBZView
      MobileRemoteControl
    )
    foreach(plugin ${_disable_plugins})
      list(APPEND extra_cmake_args -DPARAVIEW_BUILD_PLUGIN_${plugin}:BOOL=FALSE)
    endforeach()
    # Force off as visit bridge currently hard depends on OpenGL
    set(visitbridge_ENABLED FALSE)
endif()

if (APPLE)
  # We are having issues building mpi4py with Python 2.6 on Mac OSX. Hence,
  # disable it for now.
  # We set the VTK_REQUIRED_OBJCXX_FLAGS to nothing to work around the
  # fact that the current version of vtk tries to enable objc garbage collection
  # which has been removed in XCode 5
  list (APPEND extra_cmake_args
        -DPARAVIEW_USE_SYSTEM_MPI4PY:BOOL=ON
        -DUSE_COMPILER_HIDDEN_VISIBILITY:BOOL=OFF
        -DVTK_REQUIRED_OBJCXX_FLAGS:STRING=)
endif()

if (UNIX AND NOT APPLE)
  list (APPEND extra_cmake_args -DCMAKE_INSTALL_RPATH_USE_LINK_PATH:BOOL=FALSE)
endif()

set (PARAVIEW_INSTALL_DEVELOPMENT_FILES FALSE)
if (paraviewsdk_ENABLED OR __BUILDBOT_INSTALL_LOCATION)
  set (PARAVIEW_INSTALL_DEVELOPMENT_FILES TRUE)
  list(APPEND extra_cmake_args
    -DPARAVIEW_DO_UNIX_STYLE_INSTALLS:BOOL=ON)
endif()

#this can't be quoted, since that will introduce an extra
#set of quotes into pqparaviewInitializer, and break the build
set(optional_plugins CMB_Plugin+KMLExporter_Plugin)

add_external_project(paraview
  DEPENDS
    boost
    gdal
    png
    python
    qt
    # Doesn't work for OpenGL2 yet
    # visitbridge
    zlib
  DEPENDS_OPTIONAL
    freetype ffmpeg hdf5 libxml3 manta mpi silo cgns mesa osmesa netcdfcpp
  CMAKE_ARGS
    -DBUILD_SHARED_LIBS:BOOL=ON
    -DBUILD_TESTING:BOOL=OFF
    -DPARAVIEW_BUILD_PLUGIN_CoProcessingScriptGenerator:BOOL=ON
    # Disabled as new CMB doesn't use it, and it is not available in GL2.
    # -DPARAVIEW_BUILD_PLUGIN_EyeDomeLighting:BOOL=ON
    -DPARAVIEW_BUILD_PLUGIN_MantaView:BOOL=${manta_ENABLED}
    -DPARAVIEW_BUILD_QT_GUI:BOOL=${qt_ENABLED}
    -DPARAVIEW_ENABLE_FFMPEG:BOOL=${ffmpeg_ENABLED}
    -DPARAVIEW_ENABLE_PYTHON:BOOL=${python_ENABLED}
    -DPARAVIEW_ENABLE_WEB:BOOL=OFF
    -DPARAVIEW_USE_MPI:BOOL=${mpi_ENABLED}
    # Doesn't work with OpenGL2 yet, disable for now in that case.
    -DPARAVIEW_USE_VISITBRIDGE:BOOL=${visitbridge_ENABLED}
    -DVTK_USE_SYSTEM_HDF5:BOOL=${hdf5_ENABLED}
    -DVTK_USE_SYSTEM_NETCDF:BOOL=${netcdf_ENABLED}

    #Disable the SciberQuestPlugin support for cuda. If we are building on
    #a machine with cuda installed, but needs to manually set the proper
    #backend compiler (aka every linux and mac machine) this fails
    -DSQTK_CUDA:BOOL=OFF

    #currently catalyst is having problems on praxis so lets disable it for now
    -DPARAVIEW_ENABLE_CATALYST:BOOL=OFF

    #CMB needs geovis enabled to provide the kml and gdal readers
    -DModule_vtkGeovisCore:BOOL=ON
    -DModule_vtkIOGDAL:BOOL=ON
    -DModule_vtkViewsInfovis:BOOL=ON
    -DGDAL_DIR:PATH=<INSTALL_DIR>

    #CMB needs to specify external plugins so that we can let paraview properly
    #install the plugins. So we sneakily force a variable that is an
    #implementation detail of paraview branding
    -DBPC_OPTIONAL_PLUGINS:INTERNAL=${optional_plugins}
    -DPARAVIEW_INSTALL_DEVELOPMENT_FILES:BOOL=${PARAVIEW_INSTALL_DEVELOPMENT_FILES}
    # since VTK mangles all the following, I wonder if there's any point in
    # making it use system versions.
    -DVTK_USE_SYSTEM_FREETYPE:BOOL=${freetype_ENABLED}
#    -DVTK_USE_SYSTEM_LIBXML2:BOOL=${libxml2_ENABLED}
#    currently png strips rpaths don't use this in cmb, so don't use this
#    -DVTK_USE_SYSTEM_PNG:BOOL=${png_ENABLED}
    -DVTK_USE_SYSTEM_ZLIB:BOOL=${zlib_ENABLED}

    # Web documentation
    -DPARAVIEW_BUILD_WEB_DOCUMENTATION:BOOL=${PARAVIEW_BUILD_WEB_DOCUMENTATION}

    # specify the apple app install prefix. No harm in specifying it for all
    # platforms.
    -DMACOSX_APP_INSTALL_PREFIX:PATH=<INSTALL_DIR>/Applications

    #If this is true paraview doesn't properly clean the paths to system
    #libraries like netcdf
    -DCMAKE_INSTALL_RPATH_USE_LINK_PATH:BOOL=TRUE

    ${extra_cmake_args}
  LIST_SEPARATOR @
)
