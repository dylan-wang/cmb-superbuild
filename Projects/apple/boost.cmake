
if(64bit_build)
  set(am 64)
else()
  set(am 32)
endif()

set(boost_with_args --with-date_time --with-filesystem --with-system --with-thread)

set(boost_toolset)
if(CMAKE_CXX_COMPILER_ID STREQUAL "AppleClang" OR CMAKE_CXX_COMPILER_ID STREQUAL "Clang")
  set(boost_toolset "toolset=clang-darwin")
endif()

#since we don't specify a prefix for the superbuild,
#we can determine where the buld directory will be. This
#is needed as we need to wrap the build directory in quotes to
#and escape spaces in the path for boost to properly build.
string(REPLACE " " "\\ " boost_build_dir ${SuperBuild_BINARY_DIR}/boost/src/boost)
string(REPLACE " " "\\ " boost_install_dir ${install_location})

add_external_project(boost
  DEPENDS zlib
  CONFIGURE_COMMAND ./bootstrap.sh ${boost_toolset} --prefix=${boost_install_dir}
  BUILD_COMMAND ./b2 ${boost_toolset} address-model=${am} linkflags="-headerpad_max_install_names" --build-dir=${boost_build_dir} ${boost_with_args}
  INSTALL_COMMAND ./b2 ${boost_toolset} address-model=${am} linkflags="-headerpad_max_install_names" ${boost_with_args} install --prefix=${boost_install_dir}
  BUILD_IN_SOURCE 1
  )

add_extra_cmake_args(
  -DBOOST_ROOT:PATH=${install_location}
  -DBOOST_INCLUDEDIR:PATH=<INSTALL_DIR>/include/boost
  -DBOOST_LIBRARYDIR:PATH=${install_location}/lib
  -DBoost_NO_SYSTEM_PATHS:BOOL=True
  -DBoost_NO_BOOST_CMAKE:BOOL=True
  -DBoost_USE_STATIC_LIBS:BOOL=False
  -DBoost_USE_STATIC_RUNTIME:BOOL=False
)

#special mac only script to fixup boost plugin install-names so that developers
#can use the superbuild properly from other projects
add_external_project_step(install_name_fixup
  COMMENT "Fixing boost library install_names."
  COMMAND  ${CMAKE_COMMAND}
    -Dinstall_location:PATH=${install_location}
    -Dlib_name:STRING="boost"
    -P ${CMAKE_CURRENT_LIST_DIR}/fixup_library_rpath.cmake
  DEPENDEES install)
