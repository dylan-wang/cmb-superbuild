add_external_project_or_just_build_dependencies(smtk
  DEPENDS boost qt shiboken paraview remus
  CMAKE_ARGS
    ${extra_cmake_args}
    -DBUILD_SHARED_LIBS:BOOL=ON

    -DSMTK_ENABLE_QT_SUPPORT:BOOL=ON
    -DSMTK_ENABLE_VTK_SUPPORT:BOOL=ON
    -DSMTK_ENABLE_PARAVIEW_SUPPORT:BOOL=ON
    -DSMTK_ENABLE_DISCRETE_SESSION:BOOL=ON
    -DSMTK_ENABLE_EXODUS_SESSION:BOOL=ON
    -DSMTK_ENABLE_REMOTE_SESSION:BOOL=ON
    -DSMTK_ENABLE_REMUS_SUPPORT:BOOL=ON

    -DParaView_DIR:PATH=${SuperBuild_BINARY_DIR}/paraview/src/paraview-build/
    -DSMTK_ENABLE_PYTHON_WRAPPING:BOOL=${shiboken_ENABLED}
    -DBOOST_INCLUDEDIR:PATH=<INSTALL_DIR>/include/boost
    -DBOOST_LIBRARYDIR:PATH=<INSTALL_DIR>/lib

    -DMACOSX_APP_INSTALL_PREFIX:PATH=<INSTALL_DIR>/Applications
  )

STRING(REGEX REPLACE ".*/libpython([0-9\\.]+)\\.dylib" "\\1" PYTHON_VERSION "${PYTHON_LIBRARY}" )

if(shiboken_ENABLED)
  add_external_project_step(install_shiboken_python_plugin
    COMMENT "Fixing missing include files."
    COMMAND  ${CMAKE_COMMAND}

      -DBUILD_SHARED_LIBS:BOOL=ON
      -DINSTALL_DIR:PATH=<INSTALL_DIR>
      -DSMTK_BIN_DIR:PATH=${install_location}
      -DTMP_DIR:PATH=<TMP_DIR>
      -DPYTHON_VERSION:STRING=${PYTHON_VERSION}
      -P ${CMAKE_CURRENT_LIST_DIR}/install_smtk_python_plugin.cmake
    DEPENDEES install)
endif()
