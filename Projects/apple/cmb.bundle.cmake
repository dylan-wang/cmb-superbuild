# set extra cpack variables before calling paraview.bundle.common
set (CPACK_GENERATOR DragNDrop)

# include some common stub.
include(cmb.bundle.common)

#set a root folder inside the package
set(Package_Folder "CMB Suite ${cmb_version_major}.${cmb_version_minor}.${cmb_version_patch}")

include(CPack)

foreach(program ${cmb_programs_to_install})

  install(CODE
  "
  set(PV_PYTHON_LIB_INSTALL_PREFIX
  \"\${CMAKE_INSTALL_PREFIX}/${Package_Folder}/${program}.app/Contents/Python\")
  "
  COMPONENT superbuild)

  # install paraview python module into the application bundle.
  install(CODE " file(INSTALL DESTINATION \"\${PV_PYTHON_LIB_INSTALL_PREFIX}\"
                     USE_SOURCE_PERMISSIONS TYPE DIRECTORY FILES
                     \"${install_location}/Applications/paraview.app/Contents/Python/paraview\")
               "
          COMPONENT superbuild)
  install(CODE " file(GLOB vtk-so \"${install_location}/Applications/paraview.app/Contents/Libraries/vtk*Python.so\")
                 file(INSTALL DESTINATION \"\${CMAKE_INSTALL_PREFIX}/${Package_Folder}/${program}.app/Contents/Libraries\"
                      USE_SOURCE_PERMISSIONS TYPE DIRECTORY FILES
                      \${vtk-so})
               "
          COMPONENT superbuild)
  install(CODE " file(GLOB vtk-pydll \"${install_location}/Applications/paraview.app/Contents/Libraries/libvtk*pv*.dylib\")
                 file(INSTALL DESTINATION \"\${CMAKE_INSTALL_PREFIX}/${Package_Folder}/${program}.app/Contents/Libraries\"
                      USE_SOURCE_PERMISSIONS TYPE DIRECTORY FILES
                      \${vtk-pydll})
               "
          COMPONENT superbuild)

  #list all the smtk session plugins so that we make sure to install any
  #library that they reference. This won't actually install those smtk session
  #plugins, that is handled by custom install rules in cmb.cmake
  install(CODE "
              file(INSTALL DESTINATION \"\${CMAKE_INSTALL_PREFIX}/${Package_Folder}\" USE_SOURCE_PERMISSIONS TYPE DIRECTORY FILES
                   \"${install_location}/Applications/${program}.app\")
              file(WRITE \"\${CMAKE_INSTALL_PREFIX}/${Package_Folder}/${program}.app/Contents/Resources/qt.conf\")
              execute_process(
                COMMAND ${CMAKE_CURRENT_LIST_DIR}/fixup_bundle.py
                        \"\${CMAKE_INSTALL_PREFIX}/${Package_Folder}/${program}.app\"
                        \"${install_location}/lib\"
                        \"${install_location}/plugins\"
                        \"${install_location}/lib/libsmtkDiscreteSession_Plugin.dylib\"
                        \"${install_location}/lib/libsmtkExodusSession_Plugin.dylib\"
                        \"${install_location}/lib/smtkCGMSession_Plugin.dylib\")
    "
    COMPONENT superbuild)

  if (numpy_ENABLED)
    # install numpy module into the application bundle.
    install(CODE "
                # install numpy
                file(GLOB numpy-root \"${install_location}/lib/python*/site-packages/numpy\")
                file(INSTALL DESTINATION \"\${PV_PYTHON_LIB_INSTALL_PREFIX}\"
                     USE_SOURCE_PERMISSIONS TYPE DIRECTORY FILES
                     \"\${numpy-root}\")
               "
          COMPONENT superbuild)
  endif()

  #-----------------------------------------------------------------------------
  if (mpi_ENABLED AND NOT USE_SYSTEM_mpi)
    # install MPI executables (the dylib are already installed by a previous rule).
    install(CODE "
       file(INSTALL
         DESTINATION \"\${CMAKE_INSTALL_PREFIX}/${program}.app/Contents/MacOS\"
         USE_SOURCE_PERMISSIONS
         FILES \"${install_location}/bin/hydra_pmi_proxy\")

       file(INSTALL
         DESTINATION \"\${CMAKE_INSTALL_PREFIX}/${program}.app/Contents/MacOS\"
         USE_SOURCE_PERMISSIONS
         FILES \"${install_location}/bin/mpiexec.hydra\")

       file(RENAME
         \"\${CMAKE_INSTALL_PREFIX}/${Package_Folder}/${program}.app/Contents/MacOS/mpiexec.hydra\"
         \"\${CMAKE_INSTALL_PREFIX}/${Package_Folder}/${program}.app/Contents/MacOS/mpiexec\")

       # Fixup MPI bundled libraries
       execute_process(
         COMMAND
           ${CMAKE_INSTALL_NAME_TOOL} -change
             \"${install_location}/lib/libmpl.1.dylib\"
             @executable_path/../Libraries/libmpl.1.dylib
             \"\${CMAKE_INSTALL_PREFIX}/${Package_Folder}/${program}.app/Contents/MacOS/mpiexec\"
       )
       execute_process(
         COMMAND
           ${CMAKE_INSTALL_NAME_TOOL} -change
             \"${install_location}/lib/libmpl.1.dylib\"
             @executable_path/../Libraries/libmpl.1.dylib
             \"\${CMAKE_INSTALL_PREFIX}/${Package_Folder}/${program}.app/Contents/MacOS/hydra_pmi_proxy\"
       )

    "
    COMPONENT superbuild)
  endif()


  #-----------------------------------------------------------------------------
  if (matplotlib_ENABLED)
    # install matplotlib module into the application bundle.
    install(CODE "
                  # install matplotlib
                  file(GLOB matplotlib-root \"${install_location}/lib/python*/site-packages/matplotlib\")
                  file(INSTALL
                    DESTINATION \"\${PV_PYTHON_LIB_INSTALL_PREFIX}\"
                    USE_SOURCE_PERMISSIONS
                    TYPE DIRECTORY
                    FILES \"\${matplotlib-root}\")

                  # install libpng (needed for matplotlib)
                  file(GLOB png-libs \"${install_location}/lib/libpng*dylib\")
                  foreach(png-lib \${png-libs})
                    file(INSTALL
                      DESTINATION
                        \"\${CMAKE_INSTALL_PREFIX}/${Package_Folder}/${program}.app/Contents/Libraries\"
                      USE_SOURCE_PERMISSIONS
                      TYPE SHARED_LIBRARY
                      FILES \"\${png-lib}\")
                  endforeach()

                  # install libfreetype (needed for matplotlib)
                  file(GLOB freetype-libs \"${install_location}/lib/libfreetype*dylib\")
                  foreach(freetype-lib \${freetype-libs})
                    file(INSTALL
                      DESTINATION
                        \"\${CMAKE_INSTALL_PREFIX}/${Package_Folder}/${program}.app/Contents/Libraries\"
                      USE_SOURCE_PERMISSIONS
                      TYPE SHARED_LIBRARY
                      FILES \"\${freetype-lib}\")
                  endforeach()

                  # fixup matplotlib to find the bundled libraries.
                  execute_process(
                    COMMAND
                      ${CMAKE_INSTALL_NAME_TOOL} -change
                        libpng14.14.dylib
                        @executable_path/../Libraries/libpng14.14.dylib
                        \"\${PV_PYTHON_LIB_INSTALL_PREFIX}/matplotlib/_png.so\"
                  )
                  execute_process(
                    COMMAND
                      ${CMAKE_INSTALL_NAME_TOOL} -change
                        \"${install_location}/lib/libfreetype.6.dylib\"
                        @executable_path/../Libraries/libfreetype.6.dylib
                        \"\${PV_PYTHON_LIB_INSTALL_PREFIX}/matplotlib/ft2font.so\"
                  )
                 "
            COMPONENT superbuild)
  endif()
endforeach() #done building all the CMB App bundles



#currently molequeue doesn't install under applications after being built :()
#so we have a custom install rule for it. Also we should note that molequeue
#doesn't need to be fixed up in any form
install(CODE
        "
        file(INSTALL DESTINATION \"\${CMAKE_INSTALL_PREFIX}/${Package_Folder}\" USE_SOURCE_PERMISSIONS TYPE DIRECTORY FILES
             \"${install_location}/molequeue.app\")
        "
  COMPONENT superbuild)


add_test(NAME GenerateCMBPackage
         COMMAND ${CMAKE_CPACK_COMMAND} -G DragNDrop ${test_build_verbose}
         WORKING_DIRECTORY ${SuperBuild_BINARY_DIR})
set_tests_properties(GenerateCMBPackage PROPERTIES
                     LABELS "CMB"
                     TIMEOUT 4800)
