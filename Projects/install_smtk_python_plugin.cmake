if (UNIX) #Apple should never happen
  set (SHARED_LIBRARY_SUFFIX "so")
  set (PYTHON_SITE "lib/python2.7/site-packages")
  set (PYTHON_INSTALL "lib/paraview-${pv_version}/site-packages")
  set (LIB_SHIBOKEN_PY "lib/libshiboken-python2.7.so.1.2")
  if( NOT EXISTS ${SMTK_BIN_DIR}/${LIB_SHIBOKEN_PY} )
     set (LIB_SHIBOKEN_PY "lib/libshiboken-python2.7-dbg.so.1.2")
     if( NOT EXISTS ${SMTK_BIN_DIR}/${LIB_SHIBOKEN_PY} )
        message( SEND_ERROR "could not create smtk python package.  libshiboken is missing" )
        return()
     endif()
  endif()
elseif (WIN32)
  set (SHARED_LIBRARY_SUFFIX "pyd")
  set (PYTHON_SITE "lib/site-packages")
  set (PYTHON_INSTALL ${PYTHON_SITE})
  set (LIB_SHIBOKEN_PY "bin/shiboken-python2.7.dll")
endif()

execute_process(COMMAND ${CMAKE_COMMAND} -E remove_directory ${TMP_DIR}/SMTKPlugin)
execute_process(COMMAND ${CMAKE_COMMAND} -E make_directory ${TMP_DIR}/SMTKPlugin)

set(plugin_install_dir ${SMTK_BIN_DIR}/${PYTHON_INSTALL}/smtk)
execute_process(COMMAND ${CMAKE_COMMAND} -E make_directory ${plugin_install_dir})

set(py_files_libs ${SMTK_BIN_DIR}/lib/SMTKCorePython.${SHARED_LIBRARY_SUFFIX}
                  ${SMTK_BIN_DIR}/${LIB_SHIBOKEN_PY}
                  ${SMTK_BIN_DIR}/${PYTHON_SITE}/shiboken.${SHARED_LIBRARY_SUFFIX}
                  ${SMTK_BIN_DIR}/python/smtk/simple.py)
foreach(lib ${py_files_libs})
  execute_process(COMMAND ${CMAKE_COMMAND} -E copy ${lib} ${TMP_DIR}/SMTKPlugin)
endforeach()

execute_process(
  COMMAND ${pv_python_executable} ${CMAKE_CURRENT_LIST_DIR}/remove_code.py
          ${SMTK_BIN_DIR}/python/smtk/__init__.py
          ${TMP_DIR}/SMTKPlugin/__init__.py
          )

#okay the plugin is fixed up, now we need to install it into paraviews bundle
file(GLOB fixedUpLibs "${TMP_DIR}/SMTKPlugin/*")
foreach(lib ${fixedUpLibs})
  execute_process(COMMAND ${CMAKE_COMMAND} -E copy ${lib} ${plugin_install_dir})
endforeach()

# For linux, also copy smtk libs where standalone export app can find them
if (UNIX)
  set(STANDALONE_EXPORT_DIR ${SMTK_BIN_DIR}/${PYTHON_SITE}/smtk)
  execute_process(COMMAND ${CMAKE_COMMAND} -E make_directory ${STANDALONE_EXPORT_DIR})
  foreach(lib ${fixedUpLibs})
    execute_process(COMMAND ${CMAKE_COMMAND} -E copy ${lib} ${STANDALONE_EXPORT_DIR})
  endforeach()
endif()
