
#for windows we need to add a custom add definition of WIN32
#since some code uses that instead of _WIN32
add_external_project(vxl
  DEPENDS png
  CMAKE_ARGS
    #Needed to make sure we find the correct png lib/includes
    -DCMAKE_FIND_FRAMEWORK=LAST

    # Options turned ON
    -DBUILD_CORE_GEOMETRY=ON
    -DBUILD_CORE_IMAGING=ON
    -DBUILD_CORE_NUMERICS=ON
    -DBUILD_CORE_SERIALISATION=ON
    -DBUILD_CORE_UTILITIES=ON
    -DBUILD_OXL=ON
    -DBUILD_RPL=ON
    -DBUILD_RPL_RGTL=ON
    -DBUILD_MUL=ON
    -DBUILD_CONTRIB=ON

    # Options turned OFF

    -DBUILD_BRL=OFF
    -DBUILD_CONVERSIONS=OFF
    -DBUILD_CORE_VIDEO=OFF
    -DBUILD_EXAMPLES=OFF
    -DBUILD_FOR_VXL_DASHBOARD=OFF
    -DBUILD_GEL=OFF
    -DBUILD_MUL_TOOLS=OFF
    -DBUILD_OUL=OFF
    -DBUILD_OXL=OFF
    -DBUILD_PRIP=OFF
    -DBUILD_SHARED_LIBS=OFF
    -DBUILD_TBL=OFF
    -DBUILD_TESTING=OFF
    -DBUILD_UNMAINTAINED_LIBRARIES=OFF
    -DBUILD_VGUI=OFF
    -DCMAKE_INSTALL_PREFIX=<INSTALL_DIR>
)

if (WIN32)
  # vxl openjpg has a bug with MSVC compiler where it doesn't realize its using MSVC
  # compiler when using nmake or ninja generators. This patch fixes that.
  add_external_project_step(patch_fix_msvc
    COMMAND ${CMAKE_COMMAND} -E copy_if_different
          ${SuperBuild_PROJECTS_DIR}/patches/vxl.v3p.openjpeg2.CMakeLists.txt
          <SOURCE_DIR>/v3p/openjpeg2/CMakeLists.txt
    DEPENDEES update # do after update
    DEPENDERS patch  # do before patch
    )
endif()
