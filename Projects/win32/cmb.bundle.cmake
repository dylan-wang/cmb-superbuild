# script to "bundle" cmb.

#------------------------------------------------------------------------------
# include common stuff.

# setting PARAVIEW_INSTALL_MANUAL_PDF ensures that paraview.bundle.common
# will download and install the manual pdf.
set (PARAVIEW_INSTALL_MANUAL_PDF TRUE)

include(cmb.bundle.common)
set(CPACK_MONOLITHIC_INSTALL TRUE)

# set NSIS install specific stuff.

# URL to website providing assistance in installing your application.
set (CPACK_NSIS_HELP_LINK "http://paraview.org/Wiki/ParaView")
set (CPACK_NSIS_MENU_LINKS
  "bin/ModelBuilder.exe" "Model Builder"
  "bin/paraview.exe" "ParaView"
  "bin/pvserver.exe" "pvserver (Server)"
  "bin/pvdataserver.exe" "pvdataserver (Data-Server)"
  "bin/pvrenderserver.exe" "pvrenderserver (Render-Server)")
if (python_ENABLED)
  set (CPACK_NSIS_MENU_LINKS ${CPACK_NSIS_MENU_LINKS}
    "bin/pvpython.exe" "pvpython (Python Shell)")
endif()

#FIXME: need a pretty icon.
#set (CPACK_NSIS_MUI_ICON "${CMAKE_CURRENT_LIST_DIR}/paraview.ico")

#------------------------------------------------------------------------------

# install executables to bin. Don't use file glob since that will
# parsed at configure time, and no executables will be installed. Instead use
# install(Directory ) so that it is parsed at install time.
install(DIRECTORY "${install_location}/bin/"
        DESTINATION "bin"
        USE_SOURCE_PERMISSIONS
        COMPONENT superbuild
        REGEX "CMB.*"
        PATTERN "ModelBuilder"
        PATTERN "mesh"
        PATTERN "model"
        PATTERN "molequeue"
        PATTERN "shiboken")

# install all dlls to bin. This will install all VTK/ParaView dlls plus any
# other tool dlls that were placed in bin.
install(DIRECTORY "${install_location}/bin/"
        DESTINATION "bin"
        USE_SOURCE_PERMISSIONS
        COMPONENT superbuild
        FILES_MATCHING PATTERN "*.dll")

# install the .plugins file
install(FILES "${install_location}/bin/.plugins"
        DESTINATION "bin"
        COMPONENT superbuild)

if (shiboken_ENABLED)
  install(DIRECTORY "${install_location}/lib/site-packages/smtk"
          DESTINATION "bin/Lib/site-packages"
          USE_SOURCE_PERMISSIONS
          COMPONENT superbuild)
endif()

if (numpy_ENABLED)
  install(DIRECTORY "${install_location}/lib/site-packages/numpy"
          DESTINATION "bin/Lib/site-packages"
          USE_SOURCE_PERMISSIONS
          COMPONENT superbuild)
endif()

if (matplotlib_ENABLED)
  install(DIRECTORY "${install_location}/lib/site-packages/matplotlib"
          DESTINATION "bin/Lib/site-packages"
          USE_SOURCE_PERMISSIONS
          COMPONENT ParaView)

  # Move the png and zlib dlls into the installed matplotlib package so that it
  # can find them at runtime.
  install(DIRECTORY "${SuperBuild_BINARY_DIR}/png/src/png-build/"
          DESTINATION "bin/Lib/site-packages/matplotlib"
          USE_SOURCE_PERMISSIONS
          COMPONENT ParaView
          FILES_MATCHING PATTERN "*.dll")
  install(DIRECTORY "${SuperBuild_BINARY_DIR}/zlib/src/zlib-build/"
          DESTINATION "bin/Lib/site-packages/matplotlib"
          USE_SOURCE_PERMISSIONS
          COMPONENT ParaView
          FILES_MATCHING PATTERN "*.dll")
endif()

if (qt_ENABLED AND NOT USE_SYSTEM_qt)
  install(DIRECTORY
    # install all qt plugins (including sqllite).
    # FIXME: we can reconfigure Qt to be built with inbuilt sqllite support to
    # avoid the need for plugins.
    "${install_location}/plugins/"
    DESTINATION "bin"
    COMPONENT ParaView
    PATTERN "*.dll")
endif()

# install paraview python modules and others.
install(DIRECTORY "${install_location}/lib/paraview-${pv_version}"
        DESTINATION "lib"
        USE_SOURCE_PERMISSIONS
        COMPONENT superbuild
        PATTERN "*.lib" EXCLUDE)

#-----------------------------------------------------------------------------
if (mpi_ENABLED AND NOT USE_SYSTEM_mpi)
  # install MPI executables (the dlls are already installed by a previous rule).
  install(DIRECTORY "${install_location}/bin/"
        DESTINATION "bin"
        USE_SOURCE_PERMISSIONS
        COMPONENT ParaView
        FILES_MATCHING
          PATTERN "mpiexec.exe"
          PATTERN "mpirun.exe"
          PATTERN "ompi*.exe"
          PATTERN "opal*.exe"
          PATTERN "orte*.exe"
        )
  # install the mpi configuration files needed for mpiexec.
  install(DIRECTORY "${install_location}/share/openmpi"
          DESTINATION "share"
          USE_SOURCE_PERMISSIONS
          COMPONENT ParaView)
endif()
#-----------------------------------------------------------------------------

# install system runtimes.
set(CMAKE_INSTALL_SYSTEM_RUNTIME_DESTINATION "bin")
include(InstallRequiredSystemLibraries)

#-----------------------------------------------------------------------------
# include CPack at end so that all COMPONENTs specified in install rules are
# correctly detected.
include(CPack)

#-----------------------------------------------------------------------------
#if (mpich2_ENABLED AND NOT USE_SYSTEM_mpich2)
#  install(PROGRAMS "${install_location}/bin/mpiexec.hydra"
#    DESTINATION "lib/paraview-${pv_version}"
#    COMPONENT superbuild
#    RENAME "mpiexec")
#  foreach (hydra_exe hydra_nameserver hydra_persist hydra_pmi_proxy)
#    install(PROGRAMS "${install_location}/bin/${hydra_exe}"
#      DESTINATION "lib/paraview-${pv_version}"
#      COMPONENT superbuild)
#  endforeach()
#endif()

add_test(NAME GenerateCMBPackage-NSIS
         COMMAND ${CMAKE_CPACK_COMMAND} -G NSIS ${test_build_verbose}
         WORKING_DIRECTORY ${SuperBuild_BINARY_DIR})

add_test(NAME GenerateCMBPackage-ZIP
         COMMAND ${CMAKE_CPACK_COMMAND} -G ZIP ${test_build_verbose}
         WORKING_DIRECTORY ${SuperBuild_BINARY_DIR})

set_tests_properties(GenerateCMBPackage-NSIS
                     GenerateCMBPackage-ZIP
                     PROPERTIES
                     # needed so that tests are run on typical paraview
                     # dashboards
                     LABELS "CMB"
                     TIMEOUT 1200) # increase timeout to 20 mins.
