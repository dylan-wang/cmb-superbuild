add_external_project(zeroMQ
  CONFIGURE_COMMAND <SOURCE_DIR>/configure
    --enable-shared
    --disable-static
    --prefix=<INSTALL_DIR>
  BUILD_IN_SOURCE 1
)
